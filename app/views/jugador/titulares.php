<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <div class="starter-template">

     <h1>Titulares</h1>
     <table class="table table-striped">
      <tr>
        <td><strong>Nombre</strong></td>
        <td><strong>NAcimiento</strong></td>
        <td><strong>Puesto</strong></td>
        <td><strong>Acciones</strong></td>
      </tr>
      <?php foreach ($jugadores as $jugador): ?>
        <tr>
          <?php if (isset($_SESSION['titulares'][$jugador->id])): ?>
            <td>
              <?php echo $_SESSION['titulares'][$jugador->id]->nombre ?>
            </td>
            <td>
              <?php echo $_SESSION['titulares'][$jugador->id]->nacimiento ?>
            </td>
            <td>
              <?php echo $_SESSION['titulares'][$jugador->id]->id_puesto ?>
            </td>
            <td>
              <a class="btn btn-primary" href="/jugador/quitarTitular/<?php echo $jugador->id ?>">A chupar banquillo**</a>
            </td>
          <?php endif ?>
        </tr>
      <?php endforeach ?>
    </table>
  </div>
  <br><br><br><br><br><br>

</main><!-- /.container -->
<?php require "../app/views/parts/footer.php" ?>

</body>
<?php require "../app/views/parts/scripts.php"; ?>
</html>
