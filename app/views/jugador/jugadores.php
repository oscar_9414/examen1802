<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <div class="starter-template">

      <h1>Lista de jugadores</h1>
      <table class="table table-striped">
        <tr>
          <td><strong>Nombre</strong></td>
          <td><strong>Nacimiento</strong></td>
          <td><strong>Puesto</strong></td>
          <td><strong>Acciones</strong></td>
        </tr>
        <?php foreach ($jugadores as $jugador): ?>

          <tr>
            <td>
              <?php echo $jugador->nombre; ?>
            </td>
            <td>
              <?php echo date("d/m/Y", strtotime($jugador->nacimiento)) ?>
            </td>
            <td>
              <?php foreach ($puestos as $puesto): ?>
                <?php if ($jugador->id_puesto == $puesto->id): ?>
                  <?php echo $puesto->nombre; ?>
                <?php endif ?>
              <?php endforeach ?>
            </td>
            <td>
              <a class="btn btn-primary" href="/jugador/AñadirTitulares/<?php echo $jugador->id ?>">Titular</a>
            </td>
          </tr>
        <?php endforeach ?>
        <tr>
          <td colspan="4">
            <?php if (isset($_SESSION['error'])): ?>
             <h1> <?php echo$_SESSION['error'] ; ?>
            </h1>
          <?php endif ?>
        </td>
      </tr>
    </table>
    <a class="btn btn-primary" href="/jugador/nuevo">Nuevo</a>
    <hr>
    Páginas:
    <?php for ($i=1; $i <=$pages ; $i++) {
      if ($i != $page) : ?>
      <a href="/jugador/index?page=<?php echo $i ?>" class="btn">
        <?php echo $i ?>
      </a>
    <?php else: ?>
     <span class="btn">
      <?php echo $i ?>
    </span>
  <?php endif ?>
  <?php } ?>
</div>
<br><br><br><br><br><br>

</main><!-- /.container -->
<?php require "../app/views/parts/footer.php" ?>

</body>
<?php require "../app/views/parts/scripts.php"; ?>
</html>
