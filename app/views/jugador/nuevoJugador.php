<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <div class="starter-template">

      <h1>Inserte Datos Del Nuevo Jugador</h1>
      <form method="post" action="crearNuevo">
       <div class="form-group">
        <label>Nombre:</label>
        <input type="text" class="form-control" name="nombre">
      </div>
      <div class="form-group">
        <label>Nacimiento:  </label>
        <!-- <input type="date" class="form-control" name="birthdate"> -->
        <select name="año">
          <?php
          for($i=date('o'); $i>=1910; $i--){
            if ($i == date('o'))//Año según el número de la semana ISO-8601 Ejemplos: 1999 o 2003
            echo '<option value="'.$i.'" selected>'.$i.'</option>';
            else
              echo '<option value="'.$i.'">'.$i.'</option>';
          }
          ?>
        </select>

        <select name="mes">
          <?php
          for ($i=1; $i<=12; $i++) {
            if ($i == date('m'))//Representación numérica de una mes, con ceros iniciales 01 hasta 12
            echo '<option value="'.$i.'" selected>'.$i.'</option>';
            else
              echo '<option value="'.$i.'">'.$i.'</option>';
          }
          ?>
        </select>

        <select name="dia">
          <?php
          for ($i=1; $i<=31; $i++) {
            if ($i == date('d'))//Día del mes, 2 dígitos con ceros iniciales  01 a 31
            echo '<option value="'.$i.'" selected>'.$i.'</option>';
            else
              echo '<option value="'.$i.'">'.$i.'</option>';
          }
          ?>
        </select>
      </div>
      <div class="form-group">
        <label>Puesto:</label>
        <select name="id_puesto">
          <?php foreach ($puestos as $puesto): ?>
            <option value="<?php echo $puesto->id; ?>"><?php echo $puesto->nombre; ?></option>
          <?php endforeach ?>
        </select>
      </div>

      <div class="form-group">
        <input type="submit" class="form-control" name="nacimiento" value="Inscribir">
      </div>
    </form>

  </div>
  <br><br><br><br><br><br>

</main><!-- /.container -->
<?php require "../app/views/parts/footer.php" ?>

</body>
<?php require "../app/views/parts/scripts.php" ?>
</html>
