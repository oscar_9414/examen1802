<?php
namespace App\Models;

use PDO;
use Core\Model;

class Jugador extends Model
{

    function __construct()
    {

    }
    public function allPlayers()
    {
        $db = Jugador::db();;
        $statement = $db->query('SELECT * FROM jugadores');
        $jugadores = $statement->fetchAll(PDO::FETCH_CLASS, Jugador::class);
        return $jugadores;
    }
    public function positions()
    {
        $db = Jugador::db();;
        $statement = $db->query('SELECT * FROM puestos');
        $jugadores = $statement->fetchAll(PDO::FETCH_CLASS, Jugador::class);
        return $jugadores;
    }
    public function create()
    {
        $db = Jugador::db();
        $stmt = $db->prepare('INSERT INTO jugadores
            (nombre, nacimiento,id_puesto)
            VALUES(:nombre, :nacimiento,:id_puesto)');
        $stmt->bindValue(':nombre', $this->nombre);
        $stmt->bindValue(':nacimiento', $this->nacimiento);
        $stmt->bindValue(':id_puesto', $this->id_puesto);
        return $stmt->execute();

    }
    public function titulares()
    {

    }
    public static function paginate($size = 10){

        if(isset($_REQUEST["page"])){
            $page = (integer) $_REQUEST["page"];
        }else{
            $page = 1;
        }

        $offset = ($page - 1) * $size;

        $db = Jugador::db();

        $statement = $db->prepare('SELECT * FROM jugadores LIMIT :pagesize OFFSET :offset');
        $statement->bindValue(":pagesize", $size, PDO::PARAM_INT);
        $statement->bindValue(":offset", $offset, PDO::PARAM_INT);
        $statement->execute();
        $jugadores = $statement->fetchAll(PDO::FETCH_CLASS,Jugador::class);
        return $jugadores;
    }
    public static function rowCount()
    {
        $db = Jugador::db();

        $statement = $db->prepare('SELECT count(id) as count FROM jugadores');
        $statement->execute();
        $rowCount = $statement->fetch(PDO::FETCH_ASSOC);
        return $rowCount["count"];
    }
    public function findPlayer($id)
    {
        $db = Jugador::db();
        $stmt = $db->prepare('SELECT * FROM jugadores WHERE id=:id');
        $stmt->execute(array(':id' => $id));
        $stmt->setFetchMode(PDO::FETCH_CLASS, Jugador::class);
        $product = $stmt->fetch(PDO::FETCH_CLASS);
        return $product;
    }
}
