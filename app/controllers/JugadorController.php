<?php
namespace App\controllers;
use \App\Models\Jugador;

// require_once '../app/models/Product.php';
// require_once '../app/models/ProductType.php';
/**
*
*/
class JugadorController
{

    function __construct()
    {
        # code...
    }

    public function index()
    {
     $numero = 5;
     $jugadores = Jugador::paginate($numero);
     $rowCount = Jugador::rowCount();

     $pages = ceil($rowCount / $numero);
     isset($_REQUEST["page"]) ? $page =(int) $_REQUEST["page"] : $page = 1;

     $puestos = Jugador::positions();
       // $jugadores = Jugador::allPlayers();
     require '../app/views/jugador/jugadores.php';
 }

 public function nuevo()
 {
    $puestos = Jugador::positions();
    require '../app/views/jugador/nuevoJugador.php';
}

public function crearNuevo()
{
    $jugador = new Jugador();
    $jugador->nombre = $_REQUEST['nombre'];
    $jugador->nacimiento = $_POST['año'].'-'.$_POST['mes'].'-'.$_POST['dia'] . " " ."00:00:00";
    $jugador->id_puesto = $_REQUEST['id_puesto'];
            // echo "<pre>";
            // var_dump($jugador);
    $jugador->create();
    header('Location:/jugador');
}
public function AñadirTitulares($arguments)
{
        // echo "Estamos añadiendo titular";
    $id  = (int) $arguments[0];
    $jugador = Jugador::findPlayer($id);
    if (isset($_SESSION['titulares'][$jugador->id])) {
        $_SESSION['error'] = "El jugador ya es titular";
            // if (isset($_SESSION['error'])) {
        header('Location:/jugador');
            // }
    } else {
        unset($_SESSION['error']);
        $_SESSION['titulares'][$jugador->id]=$jugador;
        header('Location:/jugador/titulares');
    }
        // echo "<prE>";
        // var_dump($_SESSION['titulares']);
}
public function titulares()
{
    $jugadores = Jugador::allPlayers();
    require '../app/views/jugador/titulares.php';
}
public function quitarTitular($arguments)
{
    $id = (int) $arguments[0];
    unset($_SESSION['titulares'][$id]);
    header('Location:/jugador/titulares');
}
}//class
